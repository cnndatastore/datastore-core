# CNN DataStore Core #
Module loader for Node.js-based Datastore client or server

## Usage ##

# TODO: Expand this #

	var config = {
		modulesInstalledConfig: './modules.json',
		modules: {},
		log: {
			file: 'log.txt',
			level: 'warning'
		}
	};
	
	/*
	config.modules.p2p = {
		npmPkg: "./datastore_modules/p2p",
		config: {
			path: '/api/p2p'
		}
	};
	*/
	
	config.modules.httpServer = {
		npm: "./datastore_modules/http-server",
		//installed: "datastore-http-server",
		config: {
			port: 8005
		}
	};
	
	config.modules.socketioServer = {
		npm: "file:./datastore_modules/socketio-server",
		config: {
			express: 'httpServer'
		}
	};
	
	var core = require('datastore-core');
	core(config);