var npm = require('npm');
var fs = require('fs');
var path = require('path');
var Utils = require('./Utils');

function KaleidoscopeCore(config) {
	this.config = config;
	this.modules = {};

	this.config.modulesInstalledConfig = typeof this.config.modulesInstalledConfig === 'string' ? this.config.modulesInstalledConfig : path.resolve(__dirname, path.join('..', 'modules.json'));

	this.loadInstallModules(this.config.modules, this.config.modulesInstalledConfig);
}

KaleidoscopeCore.prototype.loadInstallModules = function(modulesConfig, modulesInstalledConfig) {
	var self = this;
	var modulesInstalled = this.readInstalledModules(modulesInstalledConfig);
	this.installMissingModules(modulesConfig, modulesInstalled, function(errors, modulesInstalled) {
		self.writeInstalledModules(modulesInstalled);
		self.loadModules(modulesConfig, modulesInstalled);
	});
};

KaleidoscopeCore.prototype.readInstalledModules = function(modulesInstalledConfig) {
	if(typeof modulesInstalledConfig !== 'string')
		modulesInstalledConfig = this.config.modulesInstalledConfig;

	try {
		return JSON.parse(fs.readFileSync(modulesInstalledConfig));
	}
	catch(error) {
		return {};
	}
};

KaleidoscopeCore.prototype.writeInstalledModules = function(modulesInstalled, modulesInstalledConfig) {
	if(typeof modulesInstalledConfig !== 'string')
		modulesInstalledConfig = this.config.modulesInstalledConfig;

	fs.writeFileSync(modulesInstalledConfig, JSON.stringify(modulesInstalled));
};

KaleidoscopeCore.prototype.log = function(logEntry) {
	// TODO look for logging modules
	console.error("LOG", logEntry);
};

KaleidoscopeCore.prototype.installMissingModules = function(modulesConfig, modulesInstalled, callback) {
	var installsRemaining = 0;
	var installErrors = null;
	var self = this;

	// TODO refactor to avoid callback hell
	Utils.objectForEach(modulesConfig, function(moduleConfig, moduleKey) {
		if(typeof moduleConfig.path !== 'string' && !modulesInstalled.hasOwnProperty(moduleConfig.npm)) { // install location not set in config and not already in modules.json
			installsRemaining++;
			// TODO add try/catch
			self.installNpmModule(moduleConfig.npm, function(error, moduleInstallPath) {
				if(error) {
					if(installErrors === null)
						installErrors = {};
					installErrors[moduleKey] = error;
				}
				else {
					// TODO add option to move after installation
					modulesInstalled[moduleConfig.npm] = moduleInstallPath;
				}
				installsRemaining--;
				if(installsRemaining == 0)
					callback(installErrors, modulesInstalled)
			});
		}
	});

	if(installsRemaining == 0)
		callback(installErrors, modulesInstalled);
};

KaleidoscopeCore.prototype.installNpmModule = function() {
	var npmPkg = arguments[0];
	if(arguments.length === 2 && typeof arguments[1] === "function") {// npmPkg, callback
		var callback = arguments[1];
		var basedir = ".";
	}
	else if(arguments.length === 3 && typeof arguments[1] === "string" && typeof arguments[2] === "function") {// npmPkg, basedir, callback
		var basedir = arguments[1];
		var callback = arguments[2];
	}
	else
		throw new Error("invalid_args");

	npm.load({
			loaded: false
		},
		function (err) {
			if(err)
				callback(err, null);
			else {
				try {
					npm.commands.install(basedir, [npmPkg], function (err, data) {

						if (err)
							callback(err, null);
						else {
							// TODO add error checking
							var moduleDir = data[data.length - 1][1];
							var defaultInstallDir = 'node_modules/';
							if(moduleDir.indexOf(defaultInstallDir) == 0)
								moduleDir = moduleDir.substr(defaultInstallDir.length);
							callback(null, moduleDir);
						}
					});
				}
				catch(error) {
					console.log(error);
					callback(error, null);
				}
			}
		});
};

KaleidoscopeCore.prototype.loadModule = function(moduleConfig, moduleKey, moduleLocations) {
	var framework = this;
	if((typeof moduleConfig.load === 'boolean' && moduleConfig.load) || typeof moduleConfig.load === 'undefined') {
		var moduleLocation = typeof moduleConfig.path === 'string' ? moduleConfig.path : moduleLocations[moduleConfig.npm];
		if(typeof moduleLocation === 'string') {
			try {
				framework.modules[moduleKey] = require(moduleLocation)(moduleConfig.config, framework);
			}
			catch(error) {
				// TODO add logger
				console.error("ER", moduleKey, error);
				var stack = error.stack.replace(/^[^\(]+?[\n$]/gm, '')
					.replace(/^\s+at\s+/gm, '')
					.replace(/^Object.<anonymous>\s*\(/gm, '{anonymous}()@')
					.split('\n');
				console.error(stack);
			}
		}
		else
			console.error(moduleKey, 'installDir not set'); // TODO add logger
	}
};

KaleidoscopeCore.prototype.loadModules = function(moduleConfigs, moduleLocations) {
	var framework = this;
	Utils.objectForEach(moduleConfigs, function(moduleConfig, moduleKey) {
		framework.loadModule(moduleConfig, moduleKey, moduleLocations);
	});
};

KaleidoscopeCore.prototype._rmDir = function(dirPath) {
	try { var files = fs.readdirSync(dirPath); }
	catch(e) { return; }
	if (files.length > 0)
		for (var i = 0; i < files.length; i++) {
			var filePath = dirPath + '/' + files[i];
			if (fs.statSync(filePath).isFile())
				fs.unlinkSync(filePath);
			else
				rmDir(filePath);
		}
	fs.rmdirSync(dirPath);
};

module.exports = function(config) {
	return new KaleidoscopeCore(config);
};